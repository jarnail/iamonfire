import React from "react"
import { Link } from "gatsby"

export default props => (
  <>
    <h1>{props.headerText}</h1>
    <nav>
      <Link to="/">Home</Link> {" | "}
      <Link to="/about">About</Link> {" | "}
      <Link to="/contact">Contact</Link>
    </nav>
  </>
)
