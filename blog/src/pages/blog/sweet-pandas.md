---
title: "More exciting than error"
date: "2020-02-17"
---

Nothing like being satisfied with what you do for living.

Here's a video of a panda eating sweets.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4n0xNbfJLR8" frameborder="0" allowfullscreen></iframe>
