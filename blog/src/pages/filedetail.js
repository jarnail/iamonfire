import React from "react"
import { graphql } from "gatsby"
import Layout from "./layout"

export default ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <div>
        <h1>File Details</h1>
        <table>
          <thead>
            <tr>
              <th>relativePath</th>
              <th>Size</th>
              <th>Extension</th>
              <th>Created On</th>
            </tr>
          </thead>
          <tbody>
            {data.allFile.edges.map(({ node }, index) => (
              <tr>
                <td>{node.name}</td>
                <td>{node.prettySize}</td>
                <td>{node.absolutePath}</td>
                <td>{node.birthTime}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allFile {
      edges {
        node {
          name
          birthTime(fromNow: true)
          prettySize
          absolutePath
        }
      }
    }
  }
`
