import React from "react"

const ErrorPage = props => {
  return (
    <div>
      <h1>Page Not Found</h1>
      <p>Page does not exist. Please try again.</p>
    </div>
  )
}

export default ErrorPage
